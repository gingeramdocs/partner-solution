package endpoints;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import payload.FetchURIParam;

public class EzeeEndpoints {

	public static Response fetchURI(FetchURIParam fetchURIParam) {
		return given().contentType("application/json").body(fetchURIParam).when().post(URLs.fetchURI_URL);
	}

}
