package testcases;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.google.common.collect.ImmutableMap;
import endpoints.EzeeEndpoints;
import io.qameta.allure.Story;
import io.restassured.response.Response;
import payload.FetchURIParam;

public class EzeeTestcases {
	long startTime, endTime;
	FetchURIParam fetchURIParam = new FetchURIParam();

	@BeforeSuite
	public void beforeMethod() {

		allureEnvironmentWriter(
				ImmutableMap.<String, String>builder().put("OS", "windows 11").put("Java Version", "21.0.2")
						.put("Base URL", "https://sit.jiovcip.jiolabs.com").put("Authentication", "JWT Token").build(),
				System.getProperty("user.dir") + "/allure-results/");
		startTime = System.currentTimeMillis();

	}

	@Story("Fetch URI")
	@Test(priority = 1)
	public void fetchURI() {

		System.out.println("___________FETCH URI___________");

		Response response = EzeeEndpoints.fetchURI(fetchURIParam);
		response.then().log().all().extract().response();
		Assert.assertEquals(response.statusCode(), 200);

	}

	@AfterSuite
	void afterMethod() {
		endTime = System.currentTimeMillis();
		long executionTimeMillis = endTime - startTime;
		long min = executionTimeMillis / 60000;
		long sec = (executionTimeMillis % 60000) / 1000;
		System.out.println("Customer Execution time: " + min + ":" + sec);
	}

}
